<?php

return array(
    '*' => array(
		'siteUrl' => null,
		'defaultWeekStartDay' => 0,
		//'enableCsrfProtection' => (!isset($_REQUEST['PATH_INFO']) || $_REQUEST['PATH_INFO'] != '/actions/charge/webhook/callback'),
        'omitScriptNameInUrls' => true,
        'extraAllowedFileExtensions' => 'json',
        'devMode' => true,
		'cpTrigger' => 'admin',
    ),

    'bodylandscape.dev' => array(
        'devMode' => true,
        'environmentVariables' => array(
        	'basePath' => '/Users/GraphicActivity/Sites/bodylandscape.dev/public/',
            'baseUrl'  => 'http://bodylandscape.dev',
        )
    ),

    'bodylandscape.co.nz' => array(
        'devMode' => false,
        'environmentVariables' => array(
        	'basePath' => '/var/www/html/',
            'baseUrl'  => 'https://bodylandscape.co.nz',
        )
    )
);
