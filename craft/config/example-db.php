<?php

return array(
    '*' => array(
        'tablePrefix' => 'craft',
        'server' => 'localhost',
    ),
    'bodylandscape.dev' => array(
    	'database' => 'body',
        'user' => 'root',
        'password' => 'xxx',
    ),
    'bodylandscape.co.nz' => array(
    	'database' => 'body',
        'user' => 'root',
        'password' => 'xxx',
    ),
);
